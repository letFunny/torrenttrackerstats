# Initial plan
We want to show movie trends by country. Specifically, we are going to look
for how many people are downloading/uploading the movie in each country. This
data is going to be presented in a nice UI with a map and some charts.

# Backend
The backend has to do most of the work. It has to search for a movie torrent,
parse the torrent, get the announce links and ask for peers. Then, it has to
ask for every peer country, aggregate the data and return it.

## Language
I chose Python for the ease of use and the disponibility of libraries. At the
end, the libraries ended up not working as I wanted, and I had to implement
most of the functionality myself. However, working with a dynamic language
allowed me to write a lot of parsing code in a very simple manner.

## Architecture
### Server
The server is a standard vanilla Flask server with some CORS options. Basically:
```python
app = Flask(__name__)
CORS(app)
```
The API has one route "/" that requires a get parameter "query" with the movie
to query. Example:
```
http://localhost:5000/?query=Grand%20Hotel%20Budapest
```

### Search
We first search on thepiratebay. I have to constantly look for mirrors as they
get taken down constantly. They have two possible API, we look for the one that
the original TPB has. I tried to do this parsing with a library but I kept
getting banned by cloudfare. At the end I have gathered the most used user
agents and rotate them:
```python
USER_AGENTS = [
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0",
]

def piratebay_search(query):
    user_agent = random.choice(USER_AGENTS)
    url='https://thepiratebay0.org/search/'+query+'/1/99/0'
    ...
    source = requests.get(url, headers={"User-Agent":user_agent}).text
    ...
```
We then get the source. For some mirrors, we can get the link from the search
page but for some others, it is an invalid link. This alone, took **a lot** of
trial and testing. At the end, I created an agnostic crawler. It clicks on the
first torrent (the one with more seeders) and gets the source. Then, we get the
magnet link.

### Magnet links
Magnet links represent the same information as torrent files but they are more
compact so it takes less hosting space for torrent sites. It has the file hash
precomputed, we have to parse the rest of the link in order to get the urls
for the trackers announce.
```python
def decode_magnet_link(link):
    # We remove the magnet:?
    tags = link.split("?")[1]
    tags = tags.split("&")
    # We remove the URL encoding
    tags = map(unquote, tags)
    # We have to strip the lines from the tr=,dn=,xt=
    tags = list(map(lambda x: x[3:], tags))
    # At this point the format is tr=udp://link
    infohash, name, trackers = tags[0], tags[1], tags[2:]
    infohash = bytearray.fromhex(infohash[9:])
    trackers = list(map(lambda x: x + "/announce", trackers))
    info = {
        "name": name,
        "infohash": infohash,
        "trackers": trackers
    }
    return info
```
We first convert the URL encoding into regular strings and then match the
contents using the [specification](https://en.wikipedia.org/wiki/Magnet_URI_scheme).

From now on, all the responses are going to be [bencoded](https://en.wikipedia.org/wiki/Bencode)
and we use the library to decode it.

### Trackers
The are two types of trackers, HTTP and UDP. Basically, both are very popular so
we have to communicate with both. For each tracker we either perform and HTTP
query like:
```python
payload = {"info_hash" : info["info_hash"],
           "peer_id" : peer_id,
           "port" : 6881,
           "upload" : 0,
           "downloaded" : 0,
           "compact" : 1}
r = requests.get(url, params=payload, timeout=3)
response = bencode.bdecode(r.content)
```
For the UDP tracker we had to do a lot more work. Checking the
[specification](http://xbtt.sourceforge.net/udp_tracker_protocol.html), we
can see that there is an initialization phase and only then, we ask for peers.
```python
# Connect request starts
buff = b""
buff += struct.pack('!q', 0x41727101980)
buff += struct.pack('!i', 0)
buff += struct.pack('!i', 123456789)
....
# Asking for peers starts
con_id = struct.unpack('!q',response[8:16])[0]
peer_id = os.urandom(20)

buff = b""
# Coonnection id
buff += struct.pack('!q', con_id)
# Action
buff += struct.pack('!i', 1)
# Transaction id
buff += struct.pack('!i', 123456)
buff += info_hash
buff += peer_id
# Downloaded
buff += struct.pack('!q', 0)
# Left
buff += struct.pack('!q', -1)
# Uploaded
buff += struct.pack('!q', 0)
# Event
## 2 = Started
buff += struct.pack('!i', 2)
# IP
buff += struct.pack('!i', 0)
# Key
buff += struct.pack('!i', 0)
# num_want
buff += struct.pack('!i', -1)
# Port
buff += struct.pack('!h', 6881)
sock.sendto(buff, conn)

# Response starts
response = sock.recvfrom(4096)[0]

struct.unpack('!i', response[0:4])
if struct.unpack('!i', response[0:4])[0] != 1:
    raise ValueError('Bad scrape response')

struct.unpack('!i',response[4:8]) # ignore transaction id
interval = struct.unpack('!i', response[8:12])
leechers = struct.unpack('!i', response[12:16])
seeders = struct.unpack('!i', response[16:20])
...
```
Then, for both protocols, we need to decode the peers because they are in a
binary format called compact mode.
```python
# The IP and port come in compact form. 4 Bytes for IP, 2 Bytes for Port
# all in network order and packed together IP+PORT
...
port_i = struct.unpack("!H", port_b)[0]
ip_i = struct.unpack("!BBBB", ip_b)
...
```

### Countries
I tried to get a free ip geolocation database working but it works to much of
a hassle, especially licensing. At the end, we sacrificed accuracy and went for
whois lookups. I tried to use a Python library but none of them worked properly
at the time. I ended up interfacing with Linux whois utility.
```python
# We call whois with a timeout
out = subprocess.check_output(["whois", ip], timeout=2)
...

# We look for lines that contain "Country: $LETTERCODE" or any variant
# on the case.
lines = out.split(b"\n")
country_lines = list(filter(lambda x: b'country' in x.lower(), lines))
# First line, second word
country = country_lines[0].split()[1]
```

### Data aggregation
We combine all the data in a dictionary and complete it with 0 for the missing
countries in order for the map to render correctly in the frontend.

## Country letter codes and politics
When getting the country codes from whois, we get a two letter abbreviation but
our map (we will cover it in frontend) three letter abbreviation. We had to
convert all the codes using a CSV. In the beginning I obtained the CSV from the
[internet](https://www.iban.com/country-codes) but we quickly ran into some
problems. As we will see later, we use a geojson database of the world which is
indexed by country name and abbreviation so our backend has to use the same name
and abbreviations. The problem is that many of the names change depending on the
political sign of the sites where you look them up. At the end, I had to curate
the list by hand and create a CSV that matches with the frontend map but indexes
by 2-letter code.

# Frontend
The first prototype I did with static HTML and CSS. The results where not very
visually appealing but I managed to create a country chloropeth map using a
little JS and D3.js. However, it was not very user friendly because, the backend
takes a lot of time to load and, when performing the request the traditional
way, the user's browser is stuck waiting for the page to load with no feedback.

I decided to completely change the scope and ditch the old code in favour of
a modern JS framework. I had to learn JS and a framework after all, so I choose
one that gave good results without delving too much on the JS ecosystem.

## Architecture
In the process of creating this page, I was learning JS, vue, HTML, CSS and
and design patterns
so the code was experiencing constant rewrites. I will try to explain each
decission broadly.

### UI components
I wanted the UI to look superb not like the HTML and CSS version. The benefit
of using a framework is that you can interact with a lot of libraries. For the
UI I choose: [buefy](https://buefy.org/).

### Little things that take several days but are not noticed
This holds a list of little UI things that took a lot of time to do but are not
noticeable.

* Icons: Getting icons into Vue was very tricky because the prefered way is to
lazy load them instead of importing the whole collection so that the page bundle
weights less at the end. The library I used is
[font-awesome](https://fontawesome.com/?from=io).

* Map: It was really frustrating that the map captured the mouse scrolling so
instead of moving the page, you would zoom the map. I had to forget the vue
abstraction and delve into the map library directly.
```html
<l-map
  ref="map"
  :center="[40, 0]"
  :zoom="3"
  style="height: 500px;"
  :options="mapOptions"
  @focus="$refs.map.mapObject.scrollWheelZoom.enable()"
  @blur="$refs.map.mapObject.scrollWheelZoom.disable()"
  ...
/>
```

* Icons sidebar: When the sidebar is not exanded, only the icons show up. This
was not automatic and needed a huge dive at css.
```cs
/* When not hovering over the sidebar, it is reduced and we dont display
the text, just the icons */
.b-sidebar .is-mini-expand:not(:hover) .menu-list li a span:nth-child(2) {
  display: none;
}
/* Style for the icon that appears on the sidebar */
.b-sidebar .sidebar-content .menu-list li a span:nth-child(1) {
  display: inline-block;
  margin-right: 10px;
}
.b-sidebar {
  /* Display above the map and the charts */
  position: absolute;
  z-index: 10;
}
```

* Modal: When requesting data, a window pops up. I had to enlarge it quite
a bit and include a search bar into a modal component which was not prepared
for that.
```html
<b-modal
  :active="isModalVisible"
  @update:active="toggleModal"
  :has-modal-card="true"
  custom-class="dialog"
>
```
```css
.dialog .modal-card {
  width: 640px !important;
  max-width: initial !important;
}
```
Reusing classes:
```html
<div class="modal-card">
  <section class="modal-card-body is-titleless">
    <b-field label="Find a film">
      <b-autocomplete
        rounded
        append-to-body
        ...
      >
        <template slot="empty">No results found</template>
      </b-autocomplete>
    </b-field>
  </section>
  <section class="modal-card-foot">
    <b-button type="is-success" :disabled="!selected" @click="fetchNewData(selected); toggleModal">Go</b-button>
  </section>
</div>
```

### Ajax
We use [axios](https://github.com/axios/axios) to perform the request into our
API. The main problems had to do with CORS and localhost. Basically, I had to
install a CORS library in the backend in order to correctly handle the headers.
I also had to properly configure both urls to be on localhost and work
correctly.

### Map
Instead of using D3.js I decided for a
[vue library](https://github.com/vue-leaflet/Vue2Leaflet) that interfaced with
[leaflet.js](https://leafletjs.com/). Using this we could add a Map like any
other vue component:
```html
<l-map 
  ref="map"
  :center="[40, 0]" 
  :zoom="3" 
  style="height: 500px;" 
  :options="mapOptions"
  @focus="$refs.map.mapObject.scrollWheelZoom.enable()"
  @blur="$refs.map.mapObject.scrollWheelZoom.disable()"
  >
    <l-choropleth-layer 
      :data="countries"
      titleKey="Country"
      idKey="Country" 
      geojsonIdKey="name" 
      :value="value" 
      :geojson="geojson" 
      :colorScale="colorScale">
        <template slot-scope="props">
          <l-info-control 
            :item="props.currentItem" 
            :unit="props.unit" 
            title="Peers" 
            placeholder="Hover over a country"/>
          <l-reference-chart 
            title="Torrent peers per country" 
            :colorScale="colorScale" 
            :min="props.min" 
            :max="props.max" 
            position="topright"/>
        </template>
    </l-choropleth-layer>
</l-map>
```

### Graphs
In the same way, we use a [library](https://vue-chartjs.org/) to interface
directly from vue. I choose [chartjs](https://www.chartjs.org/) over D3 because
it was faster, prettier and easier than D3. You can look at the components but
it is used in the same way as the map.

## Vue
Vue is a Javascript framework that forces you to change your programming model.
It is reactive meaning if you do it correctly, you can define some variable
and use it in the HTML and when the variable changes, it updates the HTML
automatically. You can look the web because there are vast amounts of tutorials
but once you have grok the gist, the twist over traditional programming, it is
much more enjoyable.

### Caveats and pitfalls
There are a lot of tricky situations in Vue where you have to have a very clear
picture of the [component lifecycle](https://vuejs.org/v2/guide/instance.html).
For instance, in order for the data to show up correctly in the bar chart,
I have to set it in the created() hook, not in mounted().

### State and rewrites
The biggest rewrite happenned when I discovered Vuex. I was handling a lot of
the state in the main Vue component and it was quickly becoming a callback
hell. After learning Vuex and really having a grasp at it, I would describe it
as a glorified global state with some magic sparkled. We have a lot of new
concepts like actions, getters, mutations, etc. I am not going to describe here
the full functionality but basically, where before we will alter some variable
in a callback, now we dispatch and action that then calls a modification that
modifies the state.

This is all hooked up with the Vue magic in that any of the components that use
it automatically get refreshed. For instance, in the bar chart I have:
```js
computed: {
  ...mapState([
    "countries"
  ]),
  ...mapGetters([
    "getTop10Countries",
    "countriesToBar",
  ])
}
```
This piece of code basically tells vue exposes the state and we consume it by
hooking the char data:
```js
this.data = countriesToBar
```
When countries change by the result of some mutation, it changes the bar chart
automatically. Lastly, here is an example on an action and a mutation (combined
but in reality in different files):
```js
fetchNewData: ({ commit }, query) => {
  axios
    .get("http://0.0.0.0:5000/", {
        params: {
            query: query,
        },
    })
    .then(response => {
      console.log(response.data);
      commit("setCountries", response.data);
      commit("setNotification", {
        message: "Data refreshed correctly",
        type: "is-success",
        duration: 3000,
      });
    })
    .catch(error => {
      console.log(error);
      commit("setNotification", {
        message: "Data could not be fetched: Network Error",
        type: "is-danger",
        duration: 5000,
      });
    });
},
```
```js
setCountries: (state, countries) => {
    state.countries = countries;
},
```

## GeoJson and Maps
We use a ready to use format called geojson that contains all the geographic
information needed to create a map. There are a lot of projects that give you
this information but not all of them have a permissive license. Lastly, as we
said before, there are a lot of politics involved maps; meaning maps dont
really follow a strict standard. This was really a problem of coordinating the
backend and frontend as we saw when changing 2 letter to 3 letter.

from bs4 import BeautifulSoup as bs
import requests
import random

# Top five most common user agents in an effort to bypass Cloudfare checks.
USER_AGENTS = [
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0",
]

def piratebay_search(query: str) -> str:
    """Performs a search in thepiratebay, gets the first result, fetches the
    magnet link and returns it.

    Arguments:
        query {str} -- [description]

    Returns:
        str -- [description]
    """
    # Random user agent
    user_agent = random.choice(USER_AGENTS)
    # Search url hardcoded, also ordered by seeders
    url='https://thepiratebay0.org/search/'+query+'/1/99/0'

    print(f"Searching for {query}")
    print(f"Using url {url}")
    # Find all the links
    source = requests.get(url, headers={"User-Agent":user_agent}).text
    soup = bs(source, "lxml")
    pages = soup.find_all('a', class_="detLink")

    # Get the first result and fetch its webpagege
    first_page = requests.get(pages[0]["href"], headers={"User-Agent":user_agent}).text
    soup = bs(first_page, "lxml")
    # Get the magnet link
    magnet = soup.find_all('a', title="Get this torrent")
    a = []
    # There should only be one
    for m in magnet:
        a.append(m['href'])
    return a

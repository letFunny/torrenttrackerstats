import os
import bencode
import struct
import socket
import binascii
import urllib

from typing import Tuple

def getSocket() -> socket.socket:
    """Create socket with timeout

    Returns:
        [socket.socket] -- Socket.
    """
    sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    sock.settimeout(3)
    return sock

def getConnection(url: str) -> Tuple[str, int]:
    """Parse a given url into host and port.

    Arguments:
        url {str} -- URL to be decoded.

    Returns:
        Tuple[str, int] -- Pair of host and port.
    """
    parsed = urllib.parse.urlparse(url)
    hostname = socket.gethostbyname(parsed.hostname)
    port = parsed.port
    return (hostname, port)

def get_peers_udp_tracker(url: str, info_hash: dict) -> bytes:
    """Communicates with UDP tracker and returns the part of the response where
    the peers are to be found.

    Arguments:
        url {str} -- URL for the tracker.
        info_hash {dict} -- Information about the torrent.

    Raises:
        ValueError: Bad URL.
        ValueError: Bad connect response.
        ValueError: Bad peer list response.

    Returns:
        bytes -- Response cropped to only contain peers.
    """
    # Connect request starts
    buff = b""
    buff += struct.pack('!q', 0x41727101980)
    buff += struct.pack('!i', 0)
    buff += struct.pack('!i', 123456789)

    sock = getSocket()
    try:
        conn = getConnection(url)
    except socket.gaierror:
        raise ValueError("Error on host {}".format(url))
    sock.sendto(buff, conn)

    # Connect Response starts
    response = sock.recvfrom(4096)[0]
    assert len(response) == 16, 'response len must be bigger than 16 but %d' % len(response)
    if struct.unpack('!i',response[0:4])[0] != 0:
        raise ValueError('Bad response')

    struct.unpack('!i',response[4:8]) # ignore transaction id

    # Asking for peers starts
    con_id = struct.unpack('!q',response[8:16])[0]
    peer_id = os.urandom(20)

    buff = b""
    # Coonnection id
    buff += struct.pack('!q', con_id)
    # Action
    buff += struct.pack('!i', 1)
    # Transaction id
    buff += struct.pack('!i', 123456)
    buff += info_hash
    buff += peer_id
    # Downloaded
    buff += struct.pack('!q', 0)
    # Left
    buff += struct.pack('!q', -1)
    # Uploaded
    buff += struct.pack('!q', 0)
    # Event
    ## 2 = Started
    buff += struct.pack('!i', 2)
    # IP
    buff += struct.pack('!i', 0)
    # Key
    buff += struct.pack('!i', 0)
    # num_want
    buff += struct.pack('!i', -1)
    # Port
    buff += struct.pack('!h', 6881)
    sock.sendto(buff, conn)

    # Response starts
    response = sock.recvfrom(4096)[0]

    struct.unpack('!i', response[0:4])
    if struct.unpack('!i', response[0:4])[0] != 1:
        raise ValueError('Bad scrape response')

    struct.unpack('!i',response[4:8]) # ignore transaction id
    # interval = struct.unpack('!i', response[8:12])
    # leechers = struct.unpack('!i', response[12:16])
    # seeders = struct.unpack('!i', response[16:20])

    return response[20:]

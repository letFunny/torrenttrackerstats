from urllib.parse import unquote

def decode_magnet_link(link: str) -> dict:
    """Given a magnet link, we construct a dictionary of torrent information:
    name, infohash and tracker list.

    Arguments:
        link {str} -- Magnet link.

    Returns:
        dict -- Information about the torrent: name, infohash, torrent list, etc.
    """
    # We remove the magnet:?
    tags = link.split("?")[1]
    tags = tags.split("&")
    # We remove the URL encoding
    tags = map(unquote, tags)
    # We have to strip the lines from the tr=,dn=,xt=
    tags = list(map(lambda x: x[3:], tags))
    # At this point the format is tr=udp://link
    infohash, name, trackers = tags[0], tags[1], tags[2:]
    infohash = bytearray.fromhex(infohash[9:])
    trackers = list(map(lambda x: x + "/announce", trackers))
    info = {
        "name": name,
        "infohash": infohash,
        "trackers": trackers
    }
    return info

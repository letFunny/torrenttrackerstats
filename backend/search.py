from get_ip_list import *
from magnet import decode_magnet_link
from thepiratebay import *

from typing import Dict

def search(what: str) -> Dict[str, int]:
    """Performs a search for a given movie name. It searches in your website of
    choice (thepiratebay), processes the torrent and creates a dictionary of
    2 letter country code and amount of peers in that country.

    Arguments:
        what {str} -- Query.

    Returns:
        Dict[str, int] -- Dictionary of countries (2 letter code) and amount of
        peers per country.
    """
    links = piratebay_search(what)
    # Gets at most five links.
    links = links[:5]
    # Decodes the magnet links into usable dictionaries.
    links = list(map(decode_magnet_link, links))
    result = {}
    for l in links:
        print(f"Processing {l['name']}")
        for t in l["trackers"]:
            print(f"Tracker: {t}")
            # Processes the torrent, asks for peers and sums the countries amounts
            # in the result dictionaries.
            dic = process_torrent(dict(announce=t, info_hash=l["infohash"]))
            if dic:
                for k in dic:
                    result[k] = result.get(k, 0) + dic[k]
    return result

from get_ip_list import process_torrent_file
from search import search
from flask import jsonify, Flask, request
from data_processing import add_all_countries, convert_country_dic
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route("/")
def json_countries():
    # Gets the query get parameter
    query = request.args.get("query")
    print(query)
    # Searches
    countries = search(query)
    # Adds the missing countries and transform them to the proper data encoding
    countries = add_all_countries(countries)
    countries = convert_country_dic(countries)
    return jsonify(countries)

app.run(host="localhost")

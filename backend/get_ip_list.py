import bencode
import hashlib
import time
import requests
import os
import sys
import socket

from udp_tracker import get_peers_udp_tracker

from typing import Tuple, Optional

def decode_compact_peer(peer: bytes) -> Tuple[str, int]:
    """Decodes a peer in compact encoding, IP and PORT packet in 6 bytes.

    Arguments:
        peer {bytes} -- Compact peer bytes.

    Returns:
        Tuple[str, int] -- Ip and port tuple.
    """
    import struct

    # The IP and port come in compact form. 4 Bytes for IP, 2 Bytes for Port
    # all in network order and packed together IP+PORT
    ip_b, port_b = peer[:4], peer[4:]
    port_i = struct.unpack("!H", port_b)[0]
    ip_i = struct.unpack("!BBBB", ip_b)
    ip_str = '.'.join(map(str, ip_b))

    return ip_str, port_i

def whois_country_ip(ip: str) -> Optional[bytes]:
    """Performs a whois lookup of the given IP. Tries to extract the country
    code and normalize it.

    Arguments:
        ip {str} -- IP.

    Returns:
        Optional[bytes] -- 2 letter country code in bytes or None if we could not
        find it.
    """
    import subprocess

    print(f"Processing ip: {ip}")

    # We call whois with a timeout
    try:
        out = subprocess.check_output(["whois", ip], timeout=2)
    except subprocess.CalledProcessError as err:
        print(f"Whois subprocess ended with error: {err}")
        return None
    except subprocess.TimeoutExpired:
        print("Whois subprocess has reached timeout")
        return None

    # We look for lines that contain "Country: $LETTERCODE" or any variant
    # on the case.
    lines = out.split(b"\n")
    country_lines = list(filter(lambda x: b'country' in x.lower(), lines))
    if country_lines:
        # First line, second word
        country = country_lines[0].split()[1]
    else:
        # Else, log the query for debugging purposes
        print(f"No country found for ip: {ip}")
        for line in lines:
            print(line)
        return None

    # Normalize into upper case.
    return country.upper()

def process_torrent_file(filename: str) -> dict:
    """Legace processing for torrent files.

    Arguments:
        filename {str} -- Path to the torrent file.

    Returns:
        [dict] -- Dictionary of information about the torrent file.
    """
    with open(filename, "rb") as fp:
        data = fp.read()
    data = bencode.bdecode(data)
    info_hash = hashlib.sha1(bencode.bencode(data['info'])).digest()
    # We dont need the information about the pieces
    del data["info"]
    return process_torrent(dict(info_hash=info_hash, announce=data["announce"]))

def process_torrent(info: dict) -> dict:
    """Processes the torrent information into a list of countries and peer amounts
    by asking the trackers then whois lookup for the countries and returns the
    dictionary.

    Arguments:
        info {dict} -- Information about the torrent.

    Returns:
        dict -- Dictionary of 2 letter country codes and peer amounts.
    """
    peer_id = os.urandom(20)

    url = info["announce"]
    print(f"URL: {url}")
    print(f"Info Hash: {info['info_hash']}")
    print(f"Peer id: {peer_id}")
    payload = {"info_hash" : info["info_hash"],
               "peer_id" : peer_id,
               "port" : 6881,
               "upload" : 0,
               "downloaded" : 0,
               "compact" : 1}
    try:
        if "udp" in url:
            peers = get_peers_udp_tracker(url, info["info_hash"])
        else:
            r = requests.get(url, params=payload, timeout=3)
            response = bencode.bdecode(r.content)
            peers = response["peers"]

    except (requests.exceptions.Timeout, socket.timeout):
        print("Tiemout while requesting the tracker")
        return None
    except bencode.exceptions.BencodeDecodeError:
        print("Invalid bencoded response from the tracker")
        print(r.content)
        return None
    except ValueError as e:
        print(e)
        return None

    # Each peer occupies 6 bytes in compact encoding.
    peers = [peers[i:i+6] for i in range(0, len(peers), 6)]
    peers = map(decode_compact_peer, peers)
    countries = {}
    for ip, _ in peers:
        country = whois_country_ip(ip)
        if country:
            # We have to get string from bytes for the dictionary to be jsonizable
            country = country.decode()
            countries[country] = countries.get(country, 0) + 1
    return countries

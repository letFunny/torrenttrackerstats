import csv
from typing import Dict

conversion_table = {}
# Reads the CSV and creates a dictionary that matches 2 letter country code to
# country name.
with open('countries_lettercode.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=",", quotechar='"')
    for row in reader:
        conversion_table[row["Code"]] = row["Name"]

def add_all_countries(countries: Dict[str, int]) -> Dict[str, int]:
    """Adds the countries which are not in the dictionary with a default value
    of zero.

    Arguments:
        countries {Dict[str, int]} -- Country dictionary indexed by two letter code.

    Returns:
        Dict[str, int] -- Dictionary with missing keys added.
    """
    for k in conversion_table:
        if not k in countries:
            countries[k] = 0
    return countries

def convert_country_dic(countries: Dict[str, int]) -> Dict[str, int]:
    """Converts each two letter code in the dictionary to a complete country
    name and then creates the structure for the frontend to consume, namely a
    list of dictionaries with amounts and country names.

    Arguments:
        countries {Dict[str, int]} -- Country dictionary (indexed by 2 letter codes)

    Returns:
        Dict[str, int] -- Resulting dictionary that is jsonizable for the frontend.
    """
    result = []
    for k in countries:
        if not k in conversion_table:
            print(f"Country code '{k}' not in conversion table")
            continue
        row = {
            "Country": conversion_table[k],
            "Amount": countries[k],
        }
        result.append(row)
    return result

# Intro
This repository is the source code for the optional assigment of Redes II at
UAM (Madrid).

# How to build and run
At the moment, there is only the development build using Flask development
server and yarn serve.

## CORS
We have added broad CORS capabilities for the flask server because at the moment
there is not defined server/proxy.

Still, when accessing the domains, you should use **localhost:port** (as we are
going to see later) instead of your local ip.

## Backend
### Dependencies
We use poetry, in order to install dependencies using poetry, run:
```sh
poetry install
```
You also need to have the whois CLI utility installed.

### Run
At the moment, we use flask development server like so:
```sh
cd backend
poetry shell
python server.py
```

## Frontend
In this project we use yarn instead of npm. In order to run the webpack
development server,
```sh
cd vue
yarn install
yarn serve
```

## Access
```
http://localhost:8080/
```

# Controls
Click the map to allow scrolling and moving. This demo is preloaded with some
real data but you can request more from the backend. On the left bar, click
New Search and select one of the options. When the data is ready, it will update
the UI.

# Screenshots

![Main window](images/main.png)

![Search window](images/search.png)

# Things learned
We are going to present here an schematic view. For a more complete document
detailing the learning process, pitfalls, etc. please refer to GROWTH.md.

## Frontend
I had to learn everything because I had not worked with HTML/CSS/JS in the past.
For me, Vue/Vuex were especially difficult. However, the most difficult was
designing the website. Lastly, it took quite a bit of experimenting with
map libraries, chart libraries, etc. until eventually landing with a good
combination.

I had to watch quite a few tutorials in:
1. Vue and Vuex
2. JS in general
3. HTML and CSS (especially properties such as position fixed, absolute, etc.
which are a pain to deal with in your first try). Grid, flex and many other
different layouts.
4. Design in general. I don't include here a copy of the first website because
it is, frankly, embarrassing. At the end, I think the result look pretty good
for a first time front end developer.
5. Lots of libraries for maps, charts, design elements, icons, etc.
6. Yarn/NPM Javascript libraries, Webpack, etc. Admitelly, even now I still
know nothing about these topics.


## Backend
I had previous experience with Python especially when dealing with bytes and
binary structures. We learnt at university how to use unpack and pack to
communicate in binary formats. As I said in GROWTH.md, I thought there were
libraries already written in Python for all the things I had to do. However,
this was not the case. I had to implement the communication with UDP trackers
and HTTP trackers which use different protocols. I also had to convert magnet
links to usable information and several other formats. The only thing I used
was a library for bencoding which would have been very tedious to do myself.

I also had to implement the whois country lookup, the country letter conversion
and the crawler. These are detailed in GROWH.md so I will only provide a summary
here. I had to crawl thepiratebay and it was hard because I would eventually
run into Cloudfare, incosistencies in the UI and many other little things.
We use the CLI tool whois in order to perform lookups and parse its
response. We then convert the 2 letter country code to three letter country
code. This last step proved to be really difficult. Lastly, we serve the
statistics over flask which was simple enough.

List:
1. UDP Tracker communication from zero.
2. HTTP Tracker communication from zero.
3. Magnet link parsing.
4. Crawling thepiratebay and evading Cloudfare.
5. Whois lookup and country code equivalences.

## General
1. Geojson and map projections.
2. How to create an API and consume it using a frotend.
3. CORS
4. User agents and other techniques to evade basic bot protection.

import Vue from 'vue';
import App from './App.vue';

import Buefy from 'buefy';
import 'buefy/dist/buefy.css';

import store from './store';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faSearch, faAngleRight, faAngleLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faSearch, faAngleRight, faAngleLeft);
Vue.component('vue-fontawesome', FontAwesomeIcon);

Vue.use(Buefy, {
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas',
});

new Vue({
  render: h => h(App),
  store,
}).$mount('#app');

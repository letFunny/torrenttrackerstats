import axios from "axios";

export default {
  fetchNewData: ({ commit }, query) => {
    axios
      .get("http://0.0.0.0:5000/", {
          params: {
              query: query,
          },
      })
      .then(response => {
        console.log(response.data);
        commit("setCountries", response.data);
        commit("setNotification", {
          message: "Data refreshed correctly",
          type: "is-success",
          duration: 3000,
        });
      })
      .catch(error => {
        console.log(error);
        commit("setNotification", {
          message: "Data could not be fetched: Network Error",
          type: "is-danger",
          duration: 5000,
        });
      });
  },
  toggleModal: ({ commit }) => {
    commit("toggleModalComponent");
  },
  toggleNotification: ({ commit }, val) => {
    commit("setNotification", val);
  },
};

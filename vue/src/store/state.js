import {countries} from "@/data/countries.js"

const countriesOrdered = countries.sort((a, b) => {
    // b - a in order to put the highest first
    return b.Amount - a.Amount
});

export default {
    countries: countriesOrdered,
    isModalVisible: false,
    notification: false,
}
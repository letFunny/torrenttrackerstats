export default {
    setCountries: (state, countries) => {
        state.countries = countries;
    },
    toggleModalComponent: (state) => {
        state.isModalVisible = !state.isModalVisible;
    },
    setNotification: (state, notification) => {
        console.log(notification);
        state.notification = notification;
    }
};
export default {
    getTop10Countries: (state) => {
        return state.countries.slice(0,10);
    },
    countriesToBar: (state, {getTop10Countries}) => {
        const countries = getTop10Countries.reverse();
        const labels = countries.map((country) => country.Country);
        const data = countries.map((country) => country.Amount);
        return {
            "labels": labels,
            "data": data
        }
    },
};